#!/bin/bash

function get_git_rev()
{
    echo `git ls-remote kde:$repo $branch | cut -f 1`
}

function get_svn_rev()
{
    echo `svn info $1 | grep "Last Changed Rev: " | cut -f 4 -d ' '`
}

function checkDownloadUptodate()
{
    local finalDestination=$1
    local result=0
    if [ "x$force" != "x-f" ]; then
        if [ -f $finalDestination ]; then
            if [ -f $versionFilePath ]; then
                fileRepoLine=`sed -n '1p' < $versionFilePath`
                if [ "$repoLine" = "$fileRepoLine" ]; then
                    rev=`get_git_rev`
                    fileRev=`sed -n '2p' < $versionFilePath`
                    if [ "$rev" = "$fileRev" ]; then
                        fileSha=`sed -n '3p' < $versionFilePath`
                        realFileSha=`sha256sum $finalDestination`
                        if [ "$fileSha" = "$realFileSha" ]; then
                            result=1
                        fi
                    fi
                fi
            fi
        fi
    fi
    return $result
}

function no_macros_injection()
{
    # return true if the module is in the exclude list, which mean
    # that it already calls the macros which deploy the translations.
    local searched_module="$1"
    grep -q "^${searched_module}$" ../no_lang_macros.txt
    return $?
}

function grabTranslations()
{
    local basename=$1
    mkdir $basename/po
    local repo=$2
    local subdir
    for subdir in ../l10n/*; do
        local podir=$subdir/messages/$l10n_module
        if test -d $podir; then
            local lang=`basename $subdir`
            cp $podir/${repo}5.po $basename/po/$lang.po 2>/dev/null
            cp $podir/${repo}5_*.po $basename/po/$lang.po 2>/dev/null
        fi
    done
}

function getTranslationFileList()
{
    local basedir="$1"
    local po_list=""
    # grab the generated pot files (including also temporary ones, but
    # they should not matter) for the module
    po_list=$(find $basedir -name 'Messages.sh' | while read messagefile; do
        cat $messagefile | grep 'podir.*\.pot[[:space:]]*\\\?$' | sed 's,.\+podir.*\/\(.\+\.po\)t[[:space:]]*\\\?$,\1,' | sort -f | uniq
    done)
    echo $po_list
}

function setupGUITranslations()
{
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local found_qm=0
    local found_po=0
    MATCH_QM='^.*_qt.po$'

    all_po_files=$(getTranslationFileList "${basedir}")

    while read lang; do
        for po_file in ${all_po_files}; do
            po_location=$(find "${basel10n}/${lang}/messages/" -name "${po_file}")
            if [ -z "${po_location}" ]; then
                continue
            fi
            if [[ "${po_file}" =~ ${MATCH_QM} ]]; then
                # files like *_qt.po are QM in disguise
                destdir=${basedir}/poqm/${lang}
                found_qm=1
            else
                destdir=${basedir}/po/${lang}
                found_po=1
            fi
            mkdir -p "${destdir}"
            cp -f "${po_location}" "${destdir}"
        done
    done < <(cat "${language_list}")
    if no_macros_injection "${module}"; then
        # the functions have been added to the source code
        found_qm=0
        found_po=0
    fi
    if [ ${found_qm} -eq 1 ]; then
        echo "ecm_install_po_files_as_qm(poqm)" >>${basedir}/CMakeLists.txt
    fi
    if [ ${found_po} -eq 1 ]; then
        # special cases:
        # - ktp-desktop-applets since it doesn't "need" k18n since it's all qml
        #   but has po files to install so needs ki18n for ki18n_install
        # - kajongg has its own implementation if KI18n, waiting for the
        #   proper binding
        if [ "${module}" = "ktp-desktop-applets" ] || [ "${module}" = "kajongg" ]; then
            echo "find_package(KF5 REQUIRED COMPONENTS I18n)" >>${basedir}/CMakeLists.txt
        fi
        echo "ki18n_install(po)" >>${basedir}/CMakeLists.txt
    fi
}

function setupDOCTranslations()
{
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local doc_dirs=""
    local man_files=""
    #echo "DIR SCRIPT: $(readlink -f $0) - $(dirname $(readlink -f $0))"
    all_doc_dirs=$(find ${basedir} -name CMakeLists.txt | \
                   xargs -n1 awk -f ../list_doc_subdirs.awk | sort -f)
    all_man_files=$(grep -ir kdoctools_create_manpage ${basedir} | \
                    sed 's/.*kdoctools_create_manpage(\(man-.\+\.docbook\) .\+$/\1/' | sort -f)
    # exception (difficult to handle in the regular way)
    if [ "${module}" = "kwave" ]; then
        all_doc_dirs="kwave"
    fi

    local translated_doc_found=0
    while read lang; do
        # set -x
        local lang_doc_dir="${basel10n}/${lang}/docs"
        if [ ! -d "${lang_doc_dir}" ]; then
            continue
        fi
        local all_lang_docs=$(cd "${lang_doc_dir}/"; find . -type d)
        for doc_dir in ${all_doc_dirs}; do
            local doc_dir_found=$(echo "${all_lang_docs}" | grep -w "${doc_dir}")
            # exceptions (difficult to generalize)
            if [ "${doc_dir}" == "khelpcenter/glossary" ]; then
                doc_dir_found=$(echo "${all_lang_docs}" | grep -w "applications/glossary")
            fi
            if [ -n "${doc_dir_found}" ]; then
                # is the directory empty? If it is, skip it
                local is_doc_empty=$(find "${lang_doc_dir}/${doc_dir_found}" -maxdepth 0 -type d -empty 2>/dev/null)
                if [ -z "${is_doc_empty}" ]; then
                    local dest_dir="${basedir}/po/${lang}/docs/${doc_dir}"
                    mkdir -p "${dest_dir}"
                    cp -r "${lang_doc_dir}/${doc_dir_found}/"* "${dest_dir}"
                    translated_doc_found=1
                fi
            fi
        done
        for man_file in ${all_man_files}; do
            man_file_found=$(cd "${lang_doc_dir}"; find . -name "${man_file}")
            local dest_dir="${basedir}/po/${lang}/docs"
            if [ -n "${man_file_found}" ]; then
                mkdir -p "${dest_dir}"
                cp -r "${lang_doc_dir}/${man_file_found}" "${dest_dir}"
                translated_doc_found=1
            fi
        done
        # set +x
    done < <(cat "${language_list}")
    if no_macros_injection "${module}"; then
        # the function has been added to the source code
        translated_doc_found=0
    fi
    if [ ${translated_doc_found} -eq 1 ]; then
        local main_cmakelists="${basedir}/CMakeLists.txt"
        echo -e "if (KF5DocTools_FOUND)\n kdoctools_install(po)\nendif()" >>${main_cmakelists}
    fi
}

function copyDataArtifacts()
{
    # Copy the element of a special directory with translated
    # artifacts (data or scripts)
    # It is an helper function for setupDataTranslations()
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local lang="$4"
    local artifacts_dir="$5"
    local out_dest_dir_var="$6"

    local lang_artifacts_dir="${basel10n}/${lang}/${artifacts_dir}"
    [ -d "${lang_artifacts_dir}" ] || return 1

    # assume that ${module} is unique
    local artifacts_dir_full=$(cd "${lang_artifacts_dir}/"; find . -type d -name "${module}" | head -n 1)
    [ -n "${artifacts_dir_full}" ] || return 1

    local artifacts_dir_base=$(basename ${artifacts_dir_full})
    local relative_dest_dir="${lang}/${artifacts_dir}/${artifacts_dir_base}"
    local dest_dir="${basedir}/po/${relative_dest_dir}"
    if [ ! -d "${dest_dir}" ]; then
        mkdir -p "${dest_dir}"
    fi
    cp -r "${lang_artifacts_dir}/${artifacts_dir_full}/"* "${dest_dir}"
    eval ${out_dest_dir_var}="${relative_dest_dir}"
    return 0
}

function setupDataTranslations()
{
    # Takes care of both data/ and scripts/
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local rel_dest_dir=""

    while read lang; do
        # process the data/ directories (and cmake_modules, if available)
        if copyDataArtifacts "${basedir}" "${basel10n}" "${module}" "${lang}" \
                             "data" "rel_dest_dir"; then
            echo "add_subdirectory(${rel_dest_dir})" >>"${basedir}/po/CMakeLists.txt"
            # copy the content of cmake_modules is available
            local cmake_modules_dir="${basel10n}/${lang}/cmake_modules"
            if [ -d "${cmake_modules_dir}" ]; then
                local cmake_dest_dir="${basedir}/po/cmake_modules"
                mkdir -p "${cmake_dest_dir}"
                cp -r "${cmake_modules_dir}/"* "${cmake_dest_dir}"
            fi
        fi
        # process the scripts/ directories (and cmake_modules, if available)
        # WARNING: this requires KI18n <=5.33 or >=5.36, or with the
        # fix which restores ki18n_install_ts_files
        if copyDataArtifacts "${basedir}" "${basel10n}" "${module}" "${lang}" \
                             "scripts" "rel_dest_dir"; then
            echo "ki18n_install_ts_files(${lang} ${rel_dest_dir})" >>"${basedir}/po/CMakeLists.txt"
        fi
    done < <(cat "${language_list}")

    if [ -e "${basedir}/po/CMakeLists.txt" ]; then
        echo -e "include(ECMOptionalAddSubdirectory)\necm_optional_add_subdirectory(po)" >>${basedir}/CMakeLists.txt
    fi
}
