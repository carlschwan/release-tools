#!/bin/bash

. config

# Make sure gpg-remote is running
gpg --digest-algo SHA512 --armor --detach-sign -o /dev/null -s config || exit 2

if [ "$release_l10n_separately" = "0" ]; then
    bash update_l10n.sh
fi

cat modules.git | while read repo branch; do
(
    bash pack.sh $repo $1
)
done

# for sending to release-team@kde.org
cat versions/* > REVISIONS_AND_HASHES
