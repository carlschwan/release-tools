# Copyright 2015, 2019 Jonathan Riddell <jr@jriddell.org>
# Copyright 2017, 2019 Adrian Chaves <adrian@chaves.io>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from os import path
from shutil import rmtree

from click import echo
from git import GitCommandError, Repo
from git.exc import InvalidGitRepositoryError, NoSuchPathError

from kde_release.cmake import project_version
from kde_release.environment import KDE_RELEASE_DIR


def products_and_branches():
    modules_path = KDE_RELEASE_DIR / 'modules.git'
    with modules_path.open() as modules_file:
        for line in modules_file:
            match = re.match(r'^(\S+)\s+(\S+)$', line)
            if match:
                yield match.group(1), match.group(2)


class ProductRuntimeError(RuntimeError):

    def __init__(self, product, error, context):
        context = '\n'.join(f'\t{key}: {value}' for key, value in context)
        message = f'{product} (error: {error})\n{context}'
        super().__init__(message)


def update_modules(srcdir, clone=True, log_missing_versions=True):
    """Iterates the products and branches in the modules.git file, updates them
    locally, and yields the path of their local clone."""
    srcdir = path.abspath(srcdir.rstrip('/'))
    for product, branch in products_and_branches():
        directory = srcdir + '/' + product

        try:
            repository = Repo(directory)
        except NoSuchPathError:
            if clone:
                try:
                    git_url = 'git://anongit.kde.org/{}'.format(product)
                    repository = Repo.clone_from(git_url, directory)
                except KeyboardInterrupt:
                    if path.exists(directory):
                        rmtree(directory, ignore_errors=True)
                    raise
                except Exception:
                    context = (('Git URL', git_url),
                               ('Target folder', directory))
                    raise ProductRuntimeError(
                        product, 'could not clone repository', context)
            else:
                context = (('Source folder', directory),)
                raise ProductRuntimeError(
                    product, 'source folder not found', context)
        except InvalidGitRepositoryError:
            context = (('Source folder', directory),)
            raise ProductRuntimeError(
                product, 'source folder is not a Git clone', context)

        try:
            repository.heads[branch].checkout()
        except GitCommandError:
            context = (('Source folder', directory),
                       ('Current branch', repository.head.name),
                       ('Branch to check out', branch))
            raise ProductRuntimeError(
                product, 'cannot checkout branch', context)
        except IndexError:
            context = (('Source folder', directory),
                       ('Current branch', repository.head.name),
                       ('Branch to check out', branch))
            raise ProductRuntimeError(
                product, 'branch does not exist', context)

        try:
            repository.remote().pull("--rebase")
        except GitCommandError:
            context = (('Source folder', directory),
                       ('Branch', branch))
            raise ProductRuntimeError(product, 'cannot pull branch', context)

        product = path.basename(directory)

        version = project_version(directory)
        if version is None:
            if log_missing_versions:
                echo(f'{product}\n'
                     f'\t(no project version found)\n'
                     f'\t\tSource folder: {directory}\n'
                     f'\t\t{directory}/CMakeLists.txt does not define a '
                     f'project version\n'
                     f'\t\tSee https://community.kde.org/'
                     f'Guidelines_and_HOWTOs/'
                     f'Application_Versioning#Bugzilla_versions')
            continue

        yield directory, product, version, branch
