# Copyright 2015, 2019 Jonathan Riddell <jr@jriddell.org>
# Copyright 2017, 2019 Adrian Chaves <adrian@chaves.io>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from shutil import rmtree
from subprocess import run, DEVNULL, PIPE
from tempfile import mkdtemp


VERSION_RE = re.compile(r'(?i):\s+project\s*\(\s*\S+\s+'
                        r'VERSION\s+(\d+(?:\.\d+(?:\.\d+(?:\.\d+)?)?)?)')


def cmake_trace(folder):
    temp_folder = mkdtemp()
    try:
        cmake_command = ['cmake', '--trace-expand', folder]
        execution = run(
            cmake_command, cwd=temp_folder, stdout=DEVNULL, stderr=PIPE)
        return execution.stderr.decode(errors='ignore')
    finally:
        rmtree(temp_folder)


def project_version(source_folder):
    match = None
    for match in VERSION_RE.finditer(cmake_trace(source_folder)):
        pass
    if not match:
        return None
    return match.group(1)
